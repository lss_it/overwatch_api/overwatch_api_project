const cors = require("cors");
const express = require("express");
const fs = require("fs");
const path = require("path");
const mysql = require("mysql2");

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(
  cors({
    origin: function (origin, callback) {
      return callback(null, true);
    },
  })
);

//CREATING CONECTION TO LABS
//READ DATA FROM JSON FILE
var filePath = path.join(__dirname + '/server_settings.json');
var serverData = JSON.parse(fs.readFileSync(filePath, 'utf-8'));

var con = mysql.createConnection({
    host: serverData.host,
    user: serverData.user,
    password: serverData.password,
    database: serverData.database,
});

//EXECUTE CONECTION
con.connect(function (err) {
    if (err) throw err;
    else {
      console.log("Conection done!");
    }
});

//GET ALL HEROES
app.get("/getHeroes", (req, res) => {
    con.query("SELECT * FROM HERO;", function (err, result, fields) {
      res.send(result);
    });
})

//GET HERO
app.get("/getHero/:ch_uuid", (req, res) => {
  let id_hero = req.params.ch_uuid
  con.query("SELECT * FROM HERO WHERE ch_uuid = " + id_hero + ";", function (err, result, fields) {
    res.send(result);
  });
})

//GET ABILITIES FROM HERO
app.get("/getAbilities/:ch_uuid", (req, res) => {
    let id_hero = req.params.ch_uuid
    con.query("SELECT ABILITY.* FROM HERO" 
    + " JOIN ABILITY ON (HERO.ch_uuid = ABILITY.id_hero)" 
    + " WHERE HERO.ch_uuid = " + id_hero +";", function (err, result, fields) {
        res.send(result);
      });
})

//GET COSMETICS FROM HERO
app.get("/getCosmetics/:ch_uuid", (req, res) => {
    let id_hero = req.params.ch_uuid
    con.query("SELECT COSMETIC.* FROM HERO" 
    + " JOIN COSMETIC ON (HERO.ch_uuid = COSMETIC.id_hero)" 
    + " WHERE HERO.ch_uuid = " + id_hero +";", function (err, result, fields) {
        res.send(result);
      });
})

//GET ACHIEVEMENTS FROM HERO
app.get("/getAchievements/:ch_uuid", (req, res) => {
    let id_hero = req.params.ch_uuid
    con.query("SELECT ACHIEVEMENT.* FROM HERO" 
    + " JOIN ACHIEVEMENT ON (HERO.ch_uuid = ACHIEVEMENT.id_hero)" 
    + " WHERE HERO.ch_uuid = " + id_hero +";", function (err, result, fields) {
        res.send(result);
      });
})

//GET HIGHLIGHTS FROM HERO
app.get("/getHighlights/:ch_uuid", (req, res) => {
    let id_hero = req.params.ch_uuid
    con.query("SELECT HIGHLIGHT_INTRO.* FROM HERO" 
    + " JOIN HIGHLIGHT_INTRO ON (HERO.ch_uuid = HIGHLIGHT_INTRO.id_hero)" 
    + " WHERE HERO.ch_uuid = " + id_hero +";", function (err, result, fields) {
        res.send(result);
      });
})

//OPEN SERVER
app.listen(PORT, () => {
    console.log("Server RUNNING [" + PORT + "]");
});